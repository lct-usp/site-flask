from flask import Flask
from flask import render_template, url_for

from flask_sqlalchemy import SQLAlchemy
from flask_mysqldb import MySQL

import babel
from datetime import date, datetime, time
from babel.dates import format_date, format_datetime, format_time, get_timezone, UTC

import json

app = Flask(__name__)

if (app.config['ENV'] == 'development'):
    app.config.from_object('config.DevelopmentConfig')
else:
    app.config.from_object('config.ProductionConfig')

db = SQLAlchemy(app)

class Article(db.Model):
    __tablename__ = 'articles'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    title_slug = db.Column(db.String(128), unique=True, nullable=False)
    description = db.Column(db.String(255), nullable=False)
    body = db.Column(db.Text, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    created = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.Integer, nullable=False)
    modified = db.Column(db.DateTime, nullable=False)
    modified_by = db.Column(db.Integer, nullable=False)
    images = db.relationship("FileStorage",
        primaryjoin="and_(Article.id==FileStorage.foreign_key, FileStorage.model == 'Articles')",
        foreign_keys="FileStorage.foreign_key")

class Feature(db.Model):
    __tablename__ = 'features'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    link = db.Column(db.String(255), nullable=False)
    article_id = db.Column(db.Integer, db.ForeignKey('article.id'), nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    created = db.Column(db.DateTime, nullable=False)
    created_by = db.Column(db.Integer, nullable=False)
    modified = db.Column(db.DateTime, nullable=False)
    modified_by = db.Column(db.Integer, nullable=False)
    article = db.relationship('Article',
        uselist=False,
        primaryjoin='Article.id==Feature.article_id',
        foreign_keys='Feature.article_id')
    images = db.relationship('FileStorage',
        primaryjoin='and_(Feature.id==FileStorage.foreign_key, FileStorage.model=="Features")',
        foreign_keys='FileStorage.foreign_key')

class FileStorage(db.Model):
    __tablename__ = 'file_storage'
    id = db.Column(db.CHAR(36), primary_key=True)
    user_id = db.Column(db.CHAR(36))
    foreign_key = db.Column(db.CHAR(36))
    model = db.Column(db.String(128))
    filename = db.Column(db.String(255))
    filesize = db.Column(db.Integer)
    mime_type = db.Column(db.String(128))
    extension = db.Column(db.String(5))
    hash = db.Column(db.String(64))
    path = db.Column(db.String(255))
    adapter = db.Column(db.String(32))
    created = db.Column(db.DateTime)
    modified = db.Column(db.DateTime)

def brazil_time(value):
    brazil = get_timezone('America/Sao_Paulo')
    format="dd/MM/YYYY HH:mm"
    return format_datetime(value, format, tzinfo=brazil, locale='pt_BR')

app.jinja_env.filters['brazil_time'] = brazil_time

def periodic_table():
    with open(app.config['APP_PATH'] + 'static/files/periodicTable.json') as f:
        data = json.load(f)
    periodicTable = {}
    for item in data['elements']:
        periodicTable[item['symbol']] = {
            'atomic_mass': item['atomic_mass'],
            'name': item['name'],
            'number': item['number'],
            'symbol': item['symbol']
        }
    return periodicTable

def handle_bad_request(e):
    return render_template('error_404.html')

app.register_error_handler(404, handle_bad_request)

@app.route('/')
def index():
    features = Feature.query.filter(Feature.active == True).order_by(Feature.id.desc()).all()
    articles = Article.query.filter(Article.active == True).order_by(Article.id.desc()).limit(4).all()
    return render_template('index.html', articles=articles, features=features)

@app.route('/noticias')
def noticias():
    articles = Article.query.filter(Article.active == True).order_by(Article.id.desc()).all()
    return render_template('noticias.html', articles=articles)

@app.route('/noticia/<title_slug>')
def noticia(title_slug):
    article = Article.query.filter(Article.title_slug == title_slug).first()
    previous = Article.query.filter(Article.id < article.id).order_by(Article.id.desc()).first()
    next = Article.query.filter(Article.id > article.id).first()
    return render_template('noticia.html', article=article, previous=previous, next=next)

@app.route('/sobre/lct')
def sobre_lct():
    return render_template('sobre/lct.html')

@app.route('/sobre/pessoal')
def sobre_pessoal():
    return render_template('sobre/pessoal.html')

@app.route('/ensino')
def ensino():
    return render_template('ensino.html')

@app.route('/pesquisa')
def pesquisa():
    return render_template('pesquisa.html')

@app.route('/multiusuario')
def multiusuario():
    return render_template('multiusuario.html')

@app.route('/servicos')
def servicos():
    return render_template('servicos.html')

@app.route('/servicos/caracterizacao-tecnologica')
def servicos_caracterizacao():
    return render_template('servicos/caracterizacao.html')

@app.route('/servicos/analises')
def servicos_analises():
    return render_template('servicos/analises.html')

@app.route('/servicos/cursos')
def servicos_cursos():
    return render_template('servicos/cursos.html')

@app.route('/servicos/assessorias-e-treinamentos')
def servicos_assessorias():
    return render_template('servicos/assessorias.html')

@app.route('/servicos/lims')
def servicos_lims():
    return render_template('servicos/lims.html')

@app.route('/infraestrutura')
def infraestrutura():
    return render_template('infraestrutura.html')

@app.route('/infraestrutura/microtomografia')
def infraestrutura_microtomografia():
    return render_template('infraestrutura/microtomografia.html')

@app.route('/infraestrutura/microscopia-eletronica-mev')
def infraestrutura_mev():
    return render_template('infraestrutura/mev.html')

@app.route('/infraestrutura/analise-de-imagens-mla')
def infraestrutura_mla():
    return render_template('infraestrutura/mla.html')

@app.route('/infraestrutura/microscopia-confocal')
def infraestrutura_confocal():
    return render_template('infraestrutura/confocal.html')

@app.route('/infraestrutura/microscopia-optica')
def infraestrutura_optica():
    return render_template('infraestrutura/optica.html')

@app.route('/infraestrutura/fluorescencia-de-raios-x')
def infraestrutura_frx():
    return render_template('infraestrutura/frx.html')

@app.route('/infraestrutura/icp-oes')
def infraestrutura_icp():
    return render_template('infraestrutura/icp.html')

@app.route('/infraestrutura/absorcao-atomica')
def infraestrutura_aas():
    return render_template('infraestrutura/aas.html')

@app.route('/infraestrutura/via-umida')
def infraestrutura_avu():
    return render_template('infraestrutura/avu.html')

@app.route('/infraestrutura/difracao-de-raios-x')
def infraestrutura_drx():
    return render_template('infraestrutura/drx.html')

@app.route('/infraestrutura/tamanho-e-forma-de-particulas')
def infraestrutura_particulas():
    return render_template('infraestrutura/particulas.html')

@app.route('/infraestrutura/porosidade-e-densidade')
def infraestrutura_porosidade():
    return render_template('infraestrutura/porosidade.html')

@app.route('/infraestrutura/separacoes-minerais')
def infraestrutura_separacoes():
    return render_template('infraestrutura/separacoes.html')

@app.route('/infraestrutura/preparacoes-fisicas')
def infraestrutura_preparacoes():
    return render_template('infraestrutura/preparacoes.html')

@app.route('/metodos/fluorescencia-de-raios-x')
def metodos_frx():
    return render_template('metodos/frx.html')

@app.route('/metodos/icp-oes')
def metodos_icp():
    return render_template('metodos/icp.html', periodicTable=periodic_table())

@app.route('/metodos/absorcao-atomica')
def metodos_aas():
    return render_template('metodos/aas.html')

@app.route('/metodos/via-umida')
def metodos_avu():
    return render_template('metodos/avu.html')

@app.route('/metodos/areia')
def metodos_areia():
    return render_template('metodos/areia.html', periodicTable=periodic_table())

@app.route('/metodos/bauxita')
def metodos_bauxita():
    return render_template('metodos/bauxita.html', periodicTable=periodic_table())

@app.route('/metodos/calcarios')
def metodos_calcarios():
    return render_template('metodos/calcarios.html', periodicTable=periodic_table())

@app.route('/metodos/cimento-agregados-e-concreto')
def metodos_cimento():
    return render_template('metodos/cimento.html', periodicTable=periodic_table())

@app.route('/metodos/fosfatos')
def metodos_fosfatos():
    return render_template('metodos/fosfatos.html', periodicTable=periodic_table())

@app.route('/metodos/minerios-e-concentrado-de-cobre')
def metodos_minerios_cobre():
    return render_template('metodos/minerios_cobre.html', periodicTable=periodic_table())

@app.route('/metodos/minerios-de-estanho-niobio-e-tantalo')
def metodos_minerios_estanho():
    return render_template('metodos/minerios_estanho.html', periodicTable=periodic_table())

@app.route('/metodos/minerios-de-ferro')
def metodos_minerios_ferro():
    return render_template('metodos/minerios_ferro.html', periodicTable=periodic_table())

@app.route('/metodos/minerios-de-manganes')
def metodos_minerios_manganes():
    return render_template('metodos/minerios_manganes.html', periodicTable=periodic_table())

@app.route('/metodos/minerios-de-ouro')
def metodos_minerios_ouro():
    return render_template('metodos/minerios_ouro.html', periodicTable=periodic_table())

@app.route('/metodos/rochas-e-solos')
def metodos_rochas():
    return render_template('metodos/rochas.html', periodicTable=periodic_table())

@app.route('/metodos/terras-raras')
def metodos_terras_raras():
    return render_template('metodos/terras_raras.html', periodicTable=periodic_table())

@app.route('/contato')
def contato():
    return render_template('contato.html')

if __name__ == "__main__":
    app.run()
